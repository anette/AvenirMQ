//测试服务端
const net = require('net');
const port = 13003;
const ip = '127.0.0.1';
this.server = new net.createServer();
this.server.on('connection', async (client) => {

    client.on('data', async (msg) => { //接收client发来的信息
        console.log("收到服务器发来消息",JSON.parse(msg));
        client.write(JSON.stringify({
            code:0,
            message:'success',
        }))
    });

    client.on('error', function (e) { //监听客户端异常
        console.log('client error' + e);
        client.end();
    });

    client.on('close', function () {
        console.log(`客户端下线了`);
    });
});
this.server.listen(port, ip, function () {
    console.log(`client3运行在：${ip}:${port}`);
});
