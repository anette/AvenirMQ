// 本程序用来测试AvenirSQL的通信和工作状态
// 之后可以转为AvenirSQL的专用连接程序
const net = require('net')
const ini = require('ini');
const libcu = require('libcu');
const a = Math.random(1).toString().slice(3, 8);
const b = Math.random(3).toString().slice(3, 8);
const c = Math.random(5).toString().slice(3, 8);
const d = Math.random(7).toString().slice(3, 8);
const e = Math.random(9).toString().slice(3, 8);

const test = {
    //默认
    init: {
        sign: "test",
        type: 'login',
        data: 'select * from test',
    },
    addUser: {
        type: 'addUser',
        data: {
            name: 'test',
            password: libcu.cipher.AesEncode('11111111'),//AES加密,
            ip: "127.0.0.1",
            port: '13000',
            key: 'a.b.rpc',
        }

    },
    delUser: {
        type: 'deleteUser',
        data: {
            name: 'test',
            password: libcu.cipher.AesEncode('11111111'),//AES加密,
        }

    },
    login: {
        type: 'login',
        data: {
            name: 'test',
            password: libcu.cipher.AesEncode('11111111'),//AES加密,
        }

    },
    updateUser: {
        type: 'updateUser',
        data: {
            name: 'test',
            password: libcu.cipher.AesEncode('123456'),//AES加密,
            ip: "127.0.0.1",
            port: '13000',
            key: 'a.b.rpc',
        }

    },
    userList: {
        type: "userList",
    },
    send: {
        type: "send",
        sign: "test",
        data: `${a}`,
    },
    send1: {
        type: "send",
        sign: "test",
        data: `${a},${b}`,
    },
    addUser1: {
        type: 'addUser',
        data: {
            name: 'parse',
            password: libcu.cipher.AesEncode('123456'),//AES加密,
            ip: "127.0.0.1",
            port: '13002',
            key: 'b.a.#',
        }

    },
    addUser2: {
        type: 'addUser',
        data: {
            name: 'test2',
            password: libcu.cipher.AesEncode('123456'),//AES加密,
            ip: "127.0.0.1",
            port: '13003',
            key: 'c.a.#',
        }

    },
    addUser3: {
        type: 'addUser',
        data: {
            name: 'test3',
            password: libcu.cipher.AesEncode('123456'),//AES加密,
            ip: "127.0.0.1",
            port: '13003',
            key: 'd.a.rpc',
        }

    },
    setKey: {
        type: 'setKey',
        data: {
            name: 'parse',
            key: 'a.b.rpc',
        }
    }
}


//要发送给服务器的数据
let send = {};

async function main() {
    let type = process.argv.splice(2);
    if (type[0] === '-help' || type[0] === '--help' || type[0] === '-h') {
        console.log(test);
        process.exit(0);
    }
    if (type.length == 0) {
        type[0] = 'init';
    }
    if (type[0] != '-s' || type[0] == '--sql') {
        send = test[type[0]];

    } else {
        send = {
            sign: "test",
            type: type[1].type,
            data: type[1],
        }
    }
    let client = new net.Socket();
    client.connect({
        host: "127.0.0.1",
        port: 52013
    })

    //客户端与服务器建立连接触发
    client.on('connect', () => {
        client.write(JSON.stringify(send));
    });

    //客户端接收数据触发
    client.on('data', function (data) {
        console.log('服务器发送的数据 : ', data.toString());
        client.end();
    });

}

main();